FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt upgrade -y

RUN apt install git python3 python3-pip -y
RUN apt install automake make cmake automake bison flex g++ libboost-all-dev libevent-dev libssl-dev libtool make pkg-config libintelrdfpmath-dev -y

RUN pip3 install conan==1.65.0  # https://gitlab.com/gitlab-org/gitlab/-/issues/389216

RUN python3 --version
RUN gcc --version
RUN conan --version
RUN base64 --version
RUN find / -name '*bidgcc000*' 2>/dev/null
